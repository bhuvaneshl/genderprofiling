package com.genderprofiling.featureextraction;

import java.io.IOException;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public final class Constants {

	private static final String TWITTER_ZIPPED_DATASET_LOCATION = "pan_2014_en_eval.zip";
	private static final String TWITTER_EXTRACTED_DATASET_LOCATION = "pan_2014_en_extracted";
	private static final String TWITTER_CLASSIFIED_FILE = "pan_2014_en_extracted/truth.txt";
	private static final String STATS_FILE_DIR = "stats/";
	private static final String BAD_WORDS_FILE = "models/bad-words.txt";
	private static final String DATA_FILE_DIR = "data/";
	private static final String OUTPUT_FILE_DIR = "output/";
	private static final String DATA_FILE_DIR_TRUTH_FILE = "data/truth.txt";
	private static final String DATA_FILE_DIR_OUTPUT_FILE = "stats/tweets.xlsx";
	private Constants() {
	}

	private static String getAbsolutepath(String relativePath) {
		Resource resource = new ClassPathResource(relativePath);
		String absolutePath = relativePath;
		try {
			absolutePath = resource.getURL().getPath();
		} catch (IOException e) {
			resource = new ClassPathResource("");
			try {
				absolutePath = resource.getURL().getPath() + "/" + relativePath;
			} catch (IOException e1) {
				//
			}
		}
		if (absolutePath.startsWith("/")) {
			absolutePath = absolutePath.substring(1);
		}
		return absolutePath;
	}

	public static String getZippedTwitterDataSetLocation() {
		return getAbsolutepath(TWITTER_ZIPPED_DATASET_LOCATION);
	}

	public static String getExtractedTwitterDataSetLocation() {
		return getAbsolutepath(TWITTER_EXTRACTED_DATASET_LOCATION);
	}

	public static String getClassfiedDataLocation() {
		return getAbsolutepath(TWITTER_CLASSIFIED_FILE);
	}
	
	public static String getStatsFilesDirectory() {
		return getAbsolutepath(STATS_FILE_DIR);
	}
	public static String getBadWordsFile() {
		return getAbsolutepath(BAD_WORDS_FILE);
	}
	public static String getDataDirectory() {
		return getAbsolutepath(DATA_FILE_DIR);
	}
	public static String getDataDirectoryTruthFile() {
		return getAbsolutepath(DATA_FILE_DIR_TRUTH_FILE);
	}
	public static String getDataDirectoryOutputFile() {
		return getAbsolutepath(DATA_FILE_DIR_OUTPUT_FILE);
	}
	
	public static String getOutputFileDirectory() {
		return getAbsolutepath(OUTPUT_FILE_DIR);
	}
}
