package com.genderprofiling.featureextraction;

import java.io.IOException;

import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class FeatureExtractionApplication {
	
	public static void main(String[] args) throws BeansException, IOException, InterruptedException {
		ConfigurableApplicationContext context = SpringApplication.run(FeatureExtractionApplication.class, args);
	//	context.getBean(FeatureExtractor.class).execute();
		 context.getBean(StatsFileCreator.class).execute();
	//	context.getBean(SubFeaturesExtraction.class).execute();
	}

}
