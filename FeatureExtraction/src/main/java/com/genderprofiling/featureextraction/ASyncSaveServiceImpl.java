package com.genderprofiling.featureextraction;

import java.util.List;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.genderprofiling.featureextraction.bean.TaskStatusBean;
import com.genderprofiling.featureextraction.entity.AdjectiveSubClassification;
import com.genderprofiling.featureextraction.entity.AdjectiveTypeDeterminer;
import com.genderprofiling.featureextraction.entity.BadWordsStats;
import com.genderprofiling.featureextraction.entity.TweetData;
import com.genderprofiling.featureextraction.entity.TweetPosClassification;
import com.genderprofiling.featureextraction.entity.TweetSentiment;
import com.genderprofiling.featureextraction.repository.AdjectiveSubClassificationRepository;
import com.genderprofiling.featureextraction.repository.AdjectiveTypeDeterminerRepository;
import com.genderprofiling.featureextraction.repository.BadWordsStatsRepository;
import com.genderprofiling.featureextraction.repository.TweetDataRepository;
import com.genderprofiling.featureextraction.repository.TweetPosClassificationRepository;
import com.genderprofiling.featureextraction.repository.TweetSentimentRepository;

@Service
public class ASyncSaveServiceImpl implements ASyncSaveService {
	public static int count = 0;
	@Autowired
	private TweetPosClassificationRepository classificationRepository;

	@Autowired
	private BadWordsStatsRepository badWordsStatsRepository;

	@Autowired
	private TweetDataRepository tweetDataRepository;

	@Autowired
	private TweetSentimentRepository tweetSentimentRepository;
	

	@Autowired
	private AdjectiveTypeDeterminerRepository adjectiveTypeDeterminerRepository;
	
	@Autowired
	private AdjectiveSubClassificationRepository adjectiveSubClassificationRepository;
	
	@Async("jdbcTaskExecutor")
	@Override
	public Future<TaskStatusBean> savePosClassificationData(List<TweetPosClassification> entities){
		TaskStatusBean taskStatusBean = new TaskStatusBean();
		try {
			System.out.println("Task savePosClassificationData " + (++count) + "Started");
			classificationRepository.saveAll(entities);
			classificationRepository.flush();
		} catch (Exception ex) {
			ex.printStackTrace();
			taskStatusBean.setError(true);
			taskStatusBean.setMessage(ex.getMessage());
		}
		
		return new AsyncResult<TaskStatusBean>(taskStatusBean);
	}
	
	@Async("jdbcTaskExecutor")
	@Override
	public Future<TaskStatusBean> saveBadWordStats(List<BadWordsStats> entities){
		TaskStatusBean taskStatusBean = new TaskStatusBean();
		try {
			System.out.println("Task saveBadWordStats " + (++count) + "Started");
			badWordsStatsRepository.saveAll(entities);
			badWordsStatsRepository.flush();
		} catch (Exception ex) {
			ex.printStackTrace();
			taskStatusBean.setError(true);
			taskStatusBean.setMessage(ex.getMessage());
		}
		
		return new AsyncResult<TaskStatusBean>(taskStatusBean);
	}
	
	@Async("jdbcTaskExecutor")
	@Override
	public Future<TaskStatusBean> saveTweetData(List<TweetData> entities){
		TaskStatusBean taskStatusBean = new TaskStatusBean();
		try {
			tweetDataRepository.saveAll(entities);
			tweetDataRepository.flush();
		} catch (Exception ex) {
			ex.printStackTrace();
			taskStatusBean.setError(true);
			taskStatusBean.setMessage(ex.getMessage());
		}
		
		return new AsyncResult<TaskStatusBean>(taskStatusBean);
	}
	
	@Async("jdbcTaskExecutor")
	@Override
	public Future<TaskStatusBean> saveSentiments(List<TweetSentiment> entities){
		TaskStatusBean taskStatusBean = new TaskStatusBean();
		try {
			System.out.println("Task saveSentiments " + (++count) + "Started");
			tweetSentimentRepository.saveAll(entities);
			tweetSentimentRepository.flush();
		} catch (Exception ex) {
			ex.printStackTrace();
			taskStatusBean.setError(true);
			taskStatusBean.setMessage(ex.getMessage());
		}
		
		return new AsyncResult<TaskStatusBean>(taskStatusBean);
	}
	
	@Async("jdbcTaskExecutor")
	@Override
	public Future<TaskStatusBean> saveAdjectiveDeterminers(List<AdjectiveTypeDeterminer> entities){
		TaskStatusBean taskStatusBean = new TaskStatusBean();
		try {
			System.out.println("Task saveAdjectiveDeterminers " + (++count) + "Started");
			adjectiveTypeDeterminerRepository.saveAll(entities);
			adjectiveTypeDeterminerRepository.flush();
		} catch (Exception ex) {
			ex.printStackTrace();
			taskStatusBean.setError(true);
			taskStatusBean.setMessage(ex.getMessage());
		}
		
		return new AsyncResult<TaskStatusBean>(taskStatusBean);
	}
	
	
	@Async("jdbcTaskExecutor")
	@Override
	public Future<TaskStatusBean> saveAdjectiveClassifications(List<AdjectiveSubClassification> entities){
		TaskStatusBean taskStatusBean = new TaskStatusBean();
		try {
			System.out.println("Task saveAdjectiveClassifications " + (++count) + "Started");
			adjectiveSubClassificationRepository.saveAll(entities);
			adjectiveSubClassificationRepository.flush();
		} catch (Exception ex) {
			ex.printStackTrace();
			taskStatusBean.setError(true);
			taskStatusBean.setMessage(ex.getMessage());
		}
		
		return new AsyncResult<TaskStatusBean>(taskStatusBean);
	}
}
