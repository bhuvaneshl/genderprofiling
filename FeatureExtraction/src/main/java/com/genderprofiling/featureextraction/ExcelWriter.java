package com.genderprofiling.featureextraction;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelWriter {
	private Workbook workbook;
	private Sheet currentSheet;
	private List<Sheet> allSheets;
	private CellStyle headerStyle;
	private List<CellStyle> cellStyles;
	private int currentSheetRowCount;
	private List<String> currentSheetHeaderColumns;

	public ExcelWriter() {
		workbook = new XSSFWorkbook();
		currentSheetHeaderColumns = new ArrayList<>();
		allSheets=new ArrayList<>();
		cellStyles = new ArrayList<>();
		headerStyle = workbook.createCellStyle();
		XSSFFont font = ((XSSFWorkbook) workbook).createFont();
		font.setFontName("Arial");
		font.setFontHeightInPoints((short) 10);
		font.setBold(true);
		headerStyle.setFont(font);
		headerStyle.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		headerStyle.setFont(font);
		setBorders(headerStyle);
		CellStyle rowEvenStyle = workbook.createCellStyle();
		rowEvenStyle.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
		rowEvenStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		setBorders(rowEvenStyle);
		CellStyle rowOddStyle = workbook.createCellStyle();
		rowOddStyle.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		rowOddStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		setBorders(rowOddStyle);
		cellStyles.add(rowEvenStyle);
		cellStyles.add(rowOddStyle);
	}

	private void setBorders(CellStyle cellStyle) {
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setBorderTop(BorderStyle.THIN);
		cellStyle.setBorderLeft(BorderStyle.THIN);
		cellStyle.setBorderRight(BorderStyle.THIN);
	}

	public void createSheet(String sheetName) {
		currentSheet = workbook.createSheet(sheetName);
		allSheets.add(currentSheet);
		currentSheetRowCount = 0;

	}

	public void addHeaderRow(List<String> columnNames) {
		Row header = currentSheet.createRow(currentSheetRowCount++);
		currentSheetHeaderColumns=new ArrayList<>(columnNames);
		for (int cellPos = 0; cellPos < columnNames.size(); cellPos++) {
			Cell headerCell = header.createCell(cellPos);
			headerCell.setCellValue(columnNames.get(cellPos));
			headerCell.setCellStyle(headerStyle);
			
		}

	}
	public void addRow(Map<String,Object> keyValues) {
		Row row = currentSheet.createRow(currentSheetRowCount);
		for (int cellPos = 0; cellPos < currentSheetHeaderColumns.size(); cellPos++) {
			Cell column = row.createCell(cellPos);
			column.setCellStyle(cellStyles.get(currentSheetRowCount%2));
			if(keyValues.containsKey(currentSheetHeaderColumns.get(cellPos))) {
				Object val=keyValues.get(currentSheetHeaderColumns.get(cellPos));
				if(val instanceof String) {
					column.setCellValue((String)val);
				}else if(val instanceof Float) { 
					column.setCellValue((Float)val);
				} else {
					column.setCellValue((Integer)val);
				}
			}else {
				column.setCellValue("");
			}
		}
		currentSheetRowCount++;
	}
	
	public void writeToFile(String fileName) {
		String fileLocation=Constants.getStatsFilesDirectory()+fileName;
		FileOutputStream outputStream;
		for(Sheet sheet:allSheets) {
			int noOfColumns = sheet.getRow(0).getPhysicalNumberOfCells();
			for(int col=0;col<noOfColumns;col++) {
				sheet.autoSizeColumn(col);
			}
		}
		try {
			outputStream = new FileOutputStream(fileLocation);
			 workbook.write(outputStream);
			 workbook.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
       
	}

}
