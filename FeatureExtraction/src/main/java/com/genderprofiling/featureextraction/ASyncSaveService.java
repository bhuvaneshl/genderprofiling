package com.genderprofiling.featureextraction;

import java.util.List;
import java.util.concurrent.Future;

import com.genderprofiling.featureextraction.bean.TaskStatusBean;
import com.genderprofiling.featureextraction.entity.AdjectiveSubClassification;
import com.genderprofiling.featureextraction.entity.AdjectiveTypeDeterminer;
import com.genderprofiling.featureextraction.entity.BadWordsStats;
import com.genderprofiling.featureextraction.entity.TweetData;
import com.genderprofiling.featureextraction.entity.TweetPosClassification;
import com.genderprofiling.featureextraction.entity.TweetSentiment;

public interface ASyncSaveService {

	public Future<TaskStatusBean> savePosClassificationData(List<TweetPosClassification> entities);

	public Future<TaskStatusBean> saveBadWordStats(List<BadWordsStats> entities);

	public Future<TaskStatusBean> saveTweetData(List<TweetData> entities);

	public Future<TaskStatusBean> saveSentiments(List<TweetSentiment> entities);

	public Future<TaskStatusBean> saveAdjectiveDeterminers(List<AdjectiveTypeDeterminer> entities);

	public Future<TaskStatusBean> saveAdjectiveClassifications(List<AdjectiveSubClassification> entities);

}
