package com.genderprofiling.featureextraction;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genderprofiling.featureextraction.extractor.SubFeaturesExtractor;


@Component
public class SubFeaturesExtraction {

	@Autowired
	private List<SubFeaturesExtractor> subFeaturesExtractors;
	
	public void execute() throws InterruptedException {
		for (SubFeaturesExtractor extractor : subFeaturesExtractors) {
			extractor.initializeData();
			extractor.extract();
		}
	}
	
}
