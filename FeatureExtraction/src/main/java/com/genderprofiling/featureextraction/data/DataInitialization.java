/**
 * 
 */
package com.genderprofiling.featureextraction.data;

/**
 * @author Bhuvanesh
 *
 */
public interface DataInitialization {

	public void savedata() throws Exception;
}
