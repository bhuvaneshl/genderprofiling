package com.genderprofiling.featureextraction.data;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.genderprofiling.featureextraction.Constants;
import com.genderprofiling.featureextraction.entity.TwitterProfile;
import com.genderprofiling.featureextraction.repository.TwitterProfileRepository;

@Service
public class TwitterDataInitialization implements DataInitialization {

	@Autowired
	private TwitterProfileRepository twitterProfileRepository;

	@Override
	public void savedata() throws IOException, URISyntaxException {
		twitterProfileRepository.deleteAll();
		Files.readAllLines(Paths.get(Constants.getClassfiedDataLocation())).stream().forEach(line -> {
			String s[] = line.split(":::");
			twitterProfileRepository.save(new TwitterProfile(s[0], s[1], s[2]));
		});

	}

}
