package com.genderprofiling.featureextraction.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.genderprofiling.featureextraction.entity.TagPosMapping;
import com.genderprofiling.featureextraction.repository.TagPosMappingRepository;
public class PosTagDataInitialization implements DataInitialization {
	@Autowired
	private TagPosMappingRepository posMappingRepository;
	
	@Override
	public void savedata() throws Exception {
		// Standford NLP generates tags as per
				// https://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos.html
				posMappingRepository.deleteAll();
				posMappingRepository.save(new TagPosMapping("conjunction", "CC", "Coordinating conjunction"));
				posMappingRepository.save(new TagPosMapping("Cardinal Number", "CD", "Cardinal number"));
				posMappingRepository.save(new TagPosMapping("Determiner", "DT", "Determiner"));
				posMappingRepository.save(new TagPosMapping("Existential there", "EX", "Existential there"));
				posMappingRepository.save(new TagPosMapping("Foreign word", "FW", "Foreign word"));
				posMappingRepository.save(new TagPosMapping("conjunction", "IN", "Preposition or subordinating conjunction"));
				posMappingRepository.save(new TagPosMapping("Adjective", "JJ", "Adjective"));
				posMappingRepository.save(new TagPosMapping("Adjective", "JJR", "Adjective, comparative"));
				posMappingRepository.save(new TagPosMapping("Adjective", "JJS", "Adjective, superlative"));
				posMappingRepository.save(new TagPosMapping("List item marker", "LS", "List item marker"));
				posMappingRepository.save(new TagPosMapping("Modal", "MD", "Modal"));
				posMappingRepository.save(new TagPosMapping("Noun", "NN", "Noun, singular or mass"));
				posMappingRepository.save(new TagPosMapping("Noun", "NNS", "Noun, plural"));
				posMappingRepository.save(new TagPosMapping("Noun", "NNP", "Proper noun, singular"));
				posMappingRepository.save(new TagPosMapping("Noun", "NNPS", "Proper noun, plural"));
				posMappingRepository.save(new TagPosMapping("Predeterminer", "PDT", "Predeterminer"));
				posMappingRepository.save(new TagPosMapping("Determiner", "POS", "Possessive ending"));
				posMappingRepository.save(new TagPosMapping("Pronoun", "PRP", "Personal pronoun"));
				posMappingRepository.save(new TagPosMapping("Pronoun", "PRP$", "Possessive pronoun"));
				posMappingRepository.save(new TagPosMapping("Adverb", "RB", "Adverb"));
				posMappingRepository.save(new TagPosMapping("Adverb", "RBR", "Adverb, comparative"));
				posMappingRepository.save(new TagPosMapping("Adverb", "RBS", "Adverb, superlative"));
				posMappingRepository.save(new TagPosMapping("Particle", "RP", "Particle"));
				posMappingRepository.save(new TagPosMapping("Symbol", "SYM", "Symbol"));
				posMappingRepository.save(new TagPosMapping("TO", "TO", "to"));
				posMappingRepository.save(new TagPosMapping("Interjection", "UH", "Interjection"));
				posMappingRepository.save(new TagPosMapping("Verb", "VB", "Verb, base form"));
				posMappingRepository.save(new TagPosMapping("Verb", "VBD", "Verb, past tense"));
				posMappingRepository.save(new TagPosMapping("Verb", "VBG", "Verb, gerund or present participle"));
				posMappingRepository.save(new TagPosMapping("Verb", "VBN", "Verb, past participle"));
				posMappingRepository.save(new TagPosMapping("Verb", "VBP", "Verb, non-3rd person singular present"));
				posMappingRepository.save(new TagPosMapping("Verb", "VBZ", "Verb, 3rd person singular present"));
				posMappingRepository.save(new TagPosMapping("interrogatives", "WDT", "Wh-determiner"));
				posMappingRepository.save(new TagPosMapping("interrogatives", "WP", "Wh-pronoun"));
				posMappingRepository.save(new TagPosMapping("interrogatives", "WP$", "Possessive wh-pronoun"));
				posMappingRepository.save(new TagPosMapping("interrogatives", "WRB", "Wh-adverb"));
				posMappingRepository.save(new TagPosMapping("TwitterSpecific", "HT", "Hash Tag"));
				posMappingRepository.save(new TagPosMapping("TwitterSpecific", "URL", "URL/WebsiteLink"));
				posMappingRepository.save(new TagPosMapping("TwitterSpecific", "USR", "USer Tagging"));
				posMappingRepository.save(new TagPosMapping("UnCategorized", "UnCategorized", "UnCategorized"));
				posMappingRepository.flush();

	}

}
