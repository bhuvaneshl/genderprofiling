package com.genderprofiling.featureextraction;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.genderprofiling.featureextraction.data.DataInitialization;
import com.genderprofiling.featureextraction.entity.BadWordsStats;
import com.genderprofiling.featureextraction.entity.BaseEntity;
import com.genderprofiling.featureextraction.entity.TweetData;
import com.genderprofiling.featureextraction.entity.TweetPosClassification;
import com.genderprofiling.featureextraction.entity.TweetSentiment;
import com.genderprofiling.featureextraction.extractor.Extractor;
import com.genderprofiling.featureextraction.utils.ZipFileHandler;
import com.genderprofiling.featureextraction.xml.Author;
import com.vdurmont.emoji.EmojiParser;

@Service
public class FeatureExtractor {

	@Autowired
	private List<DataInitialization> dataInitializers;

	@Autowired
	private List<Extractor> extractors;

	@Autowired
	private ASyncSaveService saveService;

	public void execute() throws IOException {
		extractStats();
		for (Extractor extractor : extractors) {
			if (extractor.performExtraction()) {
				extractor.buildStatsSummaryIfRequired();
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void extractStats() throws IOException {
		ZipFileHandler.unzip(Constants.getZippedTwitterDataSetLocation(),
				Constants.getExtractedTwitterDataSetLocation());
		for (Extractor extractor : extractors) {
			if (extractor.performExtraction()) {
				extractor.doInitialCleanUpIfRequired();
			}
		}

		dataInitializers.forEach(d -> {
			try {
				d.savedata();
				System.out.println(d.getClass());
			} catch (Exception e) {
				e.printStackTrace();
			}
		});

		List<BadWordsStats> badWordsStatsEntities = new ArrayList<>();
		List<TweetPosClassification> posClassificationEntities = new ArrayList<>();
		List<TweetData> tweets = new ArrayList<>();
		List<TweetSentiment> sentiments = new ArrayList<>();
		// iterate through each profile and each tweet and run the feature extractors
		Files.list(Paths.get(Constants.getExtractedTwitterDataSetLocation())).forEach(filePath -> {
			if (!filePath.endsWith("truth.txt")) {
				String twitterProfileId = filePath.getFileName().toString().replace(".xml", "");
				System.out.println(twitterProfileId);
				int tweetId = 1;
				for (String tweet : readXmlFileAndGetTweetsAsList(filePath)) {
					if(tweet.length()>150) {
						System.out.println(tweet);
					}
					 tweets.add(new TweetData(twitterProfileId, tweetId, tweet));
					String processedTweet = cleanTweet(tweet);
					for (Extractor extractor : extractors) {
						try {
							List<? extends BaseEntity> result = extractor.extractTweet(processedTweet, twitterProfileId,
									tweetId);
							if (extractor.getReturnType().equals(TweetPosClassification.class)) {
								posClassificationEntities.addAll((List<TweetPosClassification>) result);
							}
							if (extractor.getReturnType().equals(BadWordsStats.class)) {
								badWordsStatsEntities.addAll((List<BadWordsStats>) result);
							}
							if (extractor.getReturnType().equals(TweetSentiment.class)) {
								sentiments.addAll((List<TweetSentiment>) result);
							}

						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					tweetId++;
				}
				if (posClassificationEntities.size() > 5000) {
					saveService.savePosClassificationData(new ArrayList<>(posClassificationEntities));
					posClassificationEntities.clear();
				}
				if (badWordsStatsEntities.size() > 0) {
					saveService.saveBadWordStats(new ArrayList<>(badWordsStatsEntities));
					badWordsStatsEntities.clear();
				}
				if (tweets.size() >=50) {
					saveService.saveTweetData(new ArrayList<>(tweets));
					tweets.clear();
				}
				if (sentiments.size() > 1000) {
					saveService.saveSentiments(new ArrayList<>(sentiments));
					sentiments.clear();
					System.gc();
				}

			}
		});

		if (posClassificationEntities.size() > 0) {
			saveService.savePosClassificationData(new ArrayList<>(posClassificationEntities));
			posClassificationEntities.clear();
		}
		if (badWordsStatsEntities.size() > 0) {
			saveService.saveBadWordStats(new ArrayList<>(badWordsStatsEntities));
			badWordsStatsEntities.clear();
		}
		if (tweets.size() > 0) {
			saveService.saveTweetData(new ArrayList<>(tweets));
			tweets.clear();
		}
		if (sentiments.size() > 0) {
			saveService.saveSentiments(new ArrayList<>(sentiments));
			sentiments.clear();
		}
	}

	private String cleanTweet(String tweet) {
		// remove urls , hashtags and emoticons
		tweet = EmojiParser.removeAllEmojis(tweet);
		return tweet;
//		return tweet.replaceAll("https?://\\S+\\s?", "").replaceAll("http?://\\S+\\s?", "")
//				.replaceAll("#[A-Za-z0-9_]+", "").replaceAll("@[A-Za-z0-9_]+", "");
	}

	private static List<String> readXmlFileAndGetTweetsAsList(Path filePath) {
		List<String> list = new ArrayList<>();
		JAXBContext jc;
		try {
			jc = JAXBContext.newInstance(Author.class);
			final Unmarshaller unmarshaller = jc.createUnmarshaller();
			Author author = (Author) unmarshaller.unmarshal(filePath.toFile());
			if (author != null && author.getDocuments() != null && author.getDocuments().getDocument() != null) {
				list = author.getDocuments().getDocument();
			}
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return list;
	}
}
