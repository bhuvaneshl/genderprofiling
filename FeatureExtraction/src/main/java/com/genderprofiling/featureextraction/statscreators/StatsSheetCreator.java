package com.genderprofiling.featureextraction.statscreators;

import com.genderprofiling.featureextraction.ExcelWriter;

public interface StatsSheetCreator {

	public void writeStats(ExcelWriter excelWriter);
	public String getSheetName();
}
