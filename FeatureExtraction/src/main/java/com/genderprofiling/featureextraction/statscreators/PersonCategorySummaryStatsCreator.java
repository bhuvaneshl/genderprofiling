package com.genderprofiling.featureextraction.statscreators;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.genderprofiling.featureextraction.ExcelWriter;
import com.genderprofiling.featureextraction.bean.ProfilePosCategorySummary;
import com.genderprofiling.featureextraction.entity.Gender;
import com.genderprofiling.featureextraction.entity.TwitterProfile;
import com.genderprofiling.featureextraction.repository.ProfilePosClassificationSummaryRepository;
import com.genderprofiling.featureextraction.repository.TwitterProfileRepository;

@Service
public class PersonCategorySummaryStatsCreator implements StatsSheetCreator {
	@Autowired
	private ProfilePosClassificationSummaryRepository summaryRepository;

	@Autowired
	private TwitterProfileRepository profileRepository;

	@Override
	public void writeStats(ExcelWriter excelWriter) {
		List<ProfilePosCategorySummary> profileWiseSummary = summaryRepository.getProfileCategorySummary();
		List<String> posCategories = summaryRepository.getPOSCategories();
		Collections.sort(posCategories);
		posCategories.add(0, "ProfileId");
		posCategories.add(1, "Gender");
		excelWriter.addHeaderRow(posCategories);
		List<TwitterProfile> list = profileRepository.findAllByOrderByTwitterIdAsc();
		List<String> profileIds = list.stream().map(TwitterProfile::getTwitterId).collect(Collectors.toList());
		Map<String, Gender> genderMap = list.stream()
				.collect(Collectors.toMap(TwitterProfile::getTwitterId, TwitterProfile::getGender));
		list.clear();
		Map<String, List<ProfilePosCategorySummary>> profileWiseSummaryMap = profileWiseSummary.stream()
				.collect(Collectors.groupingBy(ProfilePosCategorySummary::getProfileId));
		for (String profileId : profileIds) {
			Map<String, Object> rowMap = profileWiseSummaryMap.get(profileId).stream().collect(
					Collectors.toMap(ProfilePosCategorySummary::getCategory, ProfilePosCategorySummary::getTotalCount));
			rowMap.put("ProfileId", profileId);
			rowMap.put("Gender", genderMap.get(profileId).toString());
			excelWriter.addRow(rowMap);
		}
	}

	@Override
	public String getSheetName() {
		return "POSSummary_ProfileWise";
	}

}
