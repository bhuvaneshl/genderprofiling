package com.genderprofiling.featureextraction.statscreators;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.genderprofiling.featureextraction.ExcelWriter;
import com.genderprofiling.featureextraction.bean.ProfilePosCategorySummary;
import com.genderprofiling.featureextraction.bean.ProfileStatBean;
import com.genderprofiling.featureextraction.repository.BadWordsStatsRepository;
import com.genderprofiling.featureextraction.repository.ProfilePosClassificationSummaryRepository;

@Service
public class BadWordsSummaryStatsCreator implements StatsSheetCreator {
	@Autowired
	private BadWordsStatsRepository badWordsSummaryRepository;

	@Autowired
	private ProfilePosClassificationSummaryRepository summaryRepository;

	@Override
	public void writeStats(ExcelWriter excelWriter) {
		List<ProfileStatBean> profileWiseSummary = badWordsSummaryRepository.getBadWordsSummary();
		List<ProfilePosCategorySummary> profileWiseWordCount = summaryRepository.getWordsCountByProfile();
		Map<String, Integer> wordCountMap = profileWiseWordCount.stream().collect(
				Collectors.toMap(ProfilePosCategorySummary::getProfileId, ProfilePosCategorySummary::getTotalCount));
		List<String> headers = new ArrayList<>();
		headers.add("ProfileId");
		headers.add("Gender");
		headers.add("Total Count of Words");
		headers.add("Bad Words Count");
		headers.add("Bad Words Percentage");
		headers.add("Matched Words");
		excelWriter.addHeaderRow(headers);
		for (ProfileStatBean profile : profileWiseSummary) {
			Map<String, Object> rowMap = new HashMap<>();
			rowMap.put("ProfileId", profile.getProfileId());
			rowMap.put("Gender", profile.getGender());
			rowMap.put("Total Count of Words", wordCountMap.get(profile.getProfileId()));
			rowMap.put("Bad Words Count", profile.getCount());
			rowMap.put("Bad Words Percentage",
					profile.getCount() > 0 ? (Float.valueOf(
							(float) ((profile.getCount() * 1.0)/(wordCountMap.get(profile.getProfileId()) * 1.0)  )))
							: 0);
			rowMap.put("Matched Words",
					(!StringUtils.isEmpty(profile.getMatchedData())) ? profile.getMatchedData() : "");
			excelWriter.addRow(rowMap);
		}
	}

	@Override
	public String getSheetName() {
		return "BadsSummary_ProfileWise";
	}

}
