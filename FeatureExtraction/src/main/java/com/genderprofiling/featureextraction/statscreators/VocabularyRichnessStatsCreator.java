package com.genderprofiling.featureextraction.statscreators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.genderprofiling.featureextraction.ExcelWriter;
import com.genderprofiling.featureextraction.bean.ProfilePosCategorySummary;
import com.genderprofiling.featureextraction.bean.ProfileStatBean;
import com.genderprofiling.featureextraction.entity.Gender;
import com.genderprofiling.featureextraction.entity.TwitterProfile;
import com.genderprofiling.featureextraction.repository.ProfilePosClassificationSummaryRepository;
import com.genderprofiling.featureextraction.repository.TweetPosClassificationRepository;
import com.genderprofiling.featureextraction.repository.TwitterProfileRepository;

@Service
public class VocabularyRichnessStatsCreator implements StatsSheetCreator {
	@Autowired
	private ProfilePosClassificationSummaryRepository summaryRepository;

	@Autowired
	private TwitterProfileRepository profileRepository;

	@Autowired
	private TweetPosClassificationRepository classificationRepository;

	@Override
	public void writeStats(ExcelWriter excelWriter) {
		List<ProfileStatBean> profileWiseSummary = classificationRepository.getVocabularyRichnessByProfile();
		List<ProfilePosCategorySummary> categoryWiseSummary = summaryRepository.getWordsCountByProfile();
		List<TwitterProfile> list = profileRepository.findAllByOrderByTwitterIdAsc();
		List<String> vocabCategories = new ArrayList<>();
		vocabCategories.add("Adjective");
		vocabCategories.add("Adjective, comparative");
		vocabCategories.add("Adjective, superlative");
		vocabCategories.add("Adverb");
		vocabCategories.add("Adverb, comparative");
		vocabCategories.add("Adverb, superlative");
		vocabCategories.add("Words Summary");
		Collections.sort(vocabCategories);
		vocabCategories.add(0, "ProfileId");
		vocabCategories.add(1, "Gender");
		vocabCategories.add(2, "Total Words");
		excelWriter.addHeaderRow(vocabCategories);
		List<String> profileIds = list.stream().map(TwitterProfile::getTwitterId).collect(Collectors.toList());
		Map<String, Gender> genderMap = list.stream()
				.collect(Collectors.toMap(TwitterProfile::getTwitterId, TwitterProfile::getGender));
		list.clear();
		Map<String, List<ProfileStatBean>> profileWiseSummaryMap = profileWiseSummary.stream()
				.collect(Collectors.groupingBy(ProfileStatBean::getProfileId));
		final Map<String, Integer> profileWordsMap = categoryWiseSummary.stream().collect(
				Collectors.toMap(ProfilePosCategorySummary::getProfileId, ProfilePosCategorySummary::getTotalCount));
		for (String profileId : profileIds) {
			if(profileWiseSummaryMap.get(profileId)!=null) {
			Map<String, Object> rowMap = profileWiseSummaryMap.get(profileId).stream()
					.collect(Collectors.toMap(ProfileStatBean::getGender, ProfileStatBean::getCount));
			rowMap.put("ProfileId", profileId);
			rowMap.put("Gender", genderMap.get(profileId).toString());
			rowMap.put("Total Words", profileWordsMap.get(profileId));
			Predicate<? super ProfileStatBean> isMatching = (item) -> {
				return (item.getGender().equals("Adjective, comparative") || item.getGender().equals("Adjective, superlative")
						|| item.getGender().equals("Adverb, comparative") || item.getGender().equals("Adverb, superlative"));
			};
			rowMap.put("Words Summary", profileWiseSummaryMap.get(profileId).stream().filter(isMatching)
					.map(item -> String.valueOf(item.getMatchedData())).collect(Collectors.joining()));
			excelWriter.addRow(rowMap);
			}
		}
	}

	@Override
	public String getSheetName() {
		return "VocabularyRichness";
	}

}
