package com.genderprofiling.featureextraction.bean;

public class ProfilePosCategorySummary {
	private String profileId;
	private String category;
	private Integer totalCount;
	
	public ProfilePosCategorySummary(String profileId, String category, Integer totalCount) {
		super();
		this.profileId = profileId;
		this.category = category;
		this.totalCount = totalCount;
	}
	
	
	public ProfilePosCategorySummary() {
		// TODO Auto-generated constructor stub
	}

	public String getProfileId() {
		return profileId;
	}
	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Integer getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	
	public ProfilePosCategorySummary( String category, Integer totalCount) {
		super();
		this.category = category;
		this.totalCount = totalCount;
	}
	
}
