package com.genderprofiling.featureextraction.bean;

import java.util.HashMap;
import java.util.Map;

import com.genderprofiling.featureextraction.entity.Gender;

public class ProfileBean {

	private String profileId;
	
	private Gender gender;
	
	private Map<String,FeatureAttributeBean> featureAttributes;
	
	

	private ProfileBean(String profileId, Gender gender) {
		super();
		this.profileId = profileId;
		this.gender = gender;
		featureAttributes=new HashMap<>();
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Map<String, FeatureAttributeBean> getFeatureAttributes() {
		return featureAttributes;
	}

	public void setFeatureAttributes(Map<String, FeatureAttributeBean> featureAttributes) {
		this.featureAttributes = featureAttributes;
	}
	
	
	
}
