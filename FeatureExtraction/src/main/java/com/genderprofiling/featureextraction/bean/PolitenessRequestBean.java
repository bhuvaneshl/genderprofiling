package com.genderprofiling.featureextraction.bean;

public class PolitenessRequestBean {

	private String twitterId;
	private Integer tweetId;
	private String tweet;
	public PolitenessRequestBean(String twitterId, Integer tweetId, String tweet) {
		this.twitterId = twitterId;
		this.tweetId = tweetId;
		this.tweet = tweet;
	}
	public String getTwitterId() {
		return twitterId;
	}
	public Integer getTweetId() {
		return tweetId;
	}
	public String getTweet() {
		return tweet;
	}
	
	
	
}
