package com.genderprofiling.featureextraction.bean;

public class PolitenessResponseBean {

	private String confidence;
	private boolean isrequest;
	private String label;
	private String text;
	public String getConfidence() {
		return confidence;
	}
	public void setConfidence(String confidence) {
		this.confidence = confidence;
	}
	public boolean isIsrequest() {
		return isrequest;
	}
	public void setIsrequest(boolean isrequest) {
		this.isrequest = isrequest;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	
}
