package com.genderprofiling.featureextraction.bean;

public class ProfileStatBean {

	private String profileId;

	private String gender;

	private Integer count;

	private String matchedData;

	private Float percentage;

	public ProfileStatBean(String profileId, String gender, Integer count, String matchedData) {
		super();
		this.profileId = profileId;
		this.gender = gender;
		this.count = count;
		this.matchedData = matchedData;
	}

	public ProfileStatBean(String profileId, String gender, Integer count, String matchedData, int totalCount) {
		super();
		this.profileId = profileId;
		this.gender = gender;
		this.count = count;
		this.matchedData = matchedData;
	}

	public String getProfileId() {
		return profileId;
	}

	public String getGender() {
		return gender;
	}

	public Integer getCount() {
		return count;
	}

	public String getMatchedData() {
		return matchedData;
	}

	public Float getPercentage() {
		return percentage;
	}

}
