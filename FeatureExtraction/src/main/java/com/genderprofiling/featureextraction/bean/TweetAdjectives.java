package com.genderprofiling.featureextraction.bean;

public class TweetAdjectives {

	private String tweetProfileId;
	
	private Integer tweetId;
	
	private String subCategory;
	
	private String word;
	
	private Integer wordPosition;

	public TweetAdjectives(String tweetProfileId, Integer tweetId,  Integer wordPosition,String subCategory, String word) {
		this.tweetProfileId = tweetProfileId;
		this.tweetId = tweetId;
		this.subCategory = subCategory;
		this.word = word;
		this.wordPosition=wordPosition;
	}

	public String getTweetProfileId() {
		return tweetProfileId;
	}

	public Integer getTweetId() {
		return tweetId;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public String getWord() {
		return word;
	}

	public Integer getWordPosition() {
		return wordPosition;
	}
	
	
	
}
