package com.genderprofiling.featureextraction;
import java.util.concurrent.Executor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class AsyncConfig {
	@Bean(name = "jdbcTaskExecutor")
	public Executor jdbcTaskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setMaxPoolSize(20);
		executor.setCorePoolSize(15);
		return executor;
	}
	
	@Bean(name = "extractTaskExecutor")
	public Executor extractTaskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setMaxPoolSize(40);
		executor.setCorePoolSize(20);
		return executor;
	}
}