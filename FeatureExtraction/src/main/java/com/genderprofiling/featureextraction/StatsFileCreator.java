package com.genderprofiling.featureextraction;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.genderprofiling.featureextraction.statscreators.StatsSheetCreator;

@Service
public class StatsFileCreator {
	@Autowired
	private List<StatsSheetCreator> statsSheetCreators;

	public void execute() {
		ExcelWriter excelWriter = new ExcelWriter();
		for (StatsSheetCreator creator : statsSheetCreators) {
			excelWriter.createSheet(creator.getSheetName());
			creator.writeStats(excelWriter);
		}
		excelWriter.writeToFile("Stats.xlsx");
	}
}
