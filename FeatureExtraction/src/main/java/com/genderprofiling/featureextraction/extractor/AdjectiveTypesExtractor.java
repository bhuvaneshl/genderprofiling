package com.genderprofiling.featureextraction.extractor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.genderprofiling.featureextraction.ASyncSaveService;
import com.genderprofiling.featureextraction.bean.TaskStatusBean;
import com.genderprofiling.featureextraction.bean.TweetAdjectives;
import com.genderprofiling.featureextraction.entity.AdjectiveSubClassification;
import com.genderprofiling.featureextraction.entity.AdjectiveTypeDeterminer;
import com.genderprofiling.featureextraction.entity.AdjectiveTypes;
import com.genderprofiling.featureextraction.entity.TwitterProfile;
import com.genderprofiling.featureextraction.repository.AdjectiveSubClassificationRepository;
import com.genderprofiling.featureextraction.repository.AdjectiveTypeDeterminerRepository;
import com.genderprofiling.featureextraction.repository.TweetPosClassificationRepository;
import com.genderprofiling.featureextraction.repository.TwitterProfileRepository;
import com.genderprofiling.featureextraction.utils.Conversions;

@Service
public class AdjectiveTypesExtractor implements SubFeaturesExtractor {

	@Autowired
	private ASyncSaveService saveService;

	@Autowired
	private TweetPosClassificationRepository tweetPosClassificationRepository;

	@Autowired
	private TwitterProfileRepository profileRepository;

	@Autowired
	private AdjectiveTypeDeterminerRepository adjectiveTypeDeterminerRepository;

	@Autowired
	private AdjectiveSubClassificationRepository adjectiveSubClassificationRepository;

	private static Map<String, AdjectiveTypes> adjectiveTypes;

	private static List<String> emotionalAdjectives;

	private void initialize() {
		List<AdjectiveTypeDeterminer> types = adjectiveTypeDeterminerRepository.findAll();
		adjectiveTypes = types.stream().collect(
				Collectors.toMap(AdjectiveTypeDeterminer::getWord, adj -> AdjectiveTypes.valueOf(adj.getType())));

		emotionalAdjectives = types.stream().filter(t -> t.getType().equalsIgnoreCase("EMOTIONAL"))
				.map(AdjectiveTypeDeterminer::getWord).collect(Collectors.toList());
	}

	@Override
	public void initializeData() {
		adjectiveTypeDeterminerRepository.deleteAll();
		adjectiveSubClassificationRepository.deleteAll();

		List<AdjectiveTypeDeterminer> adjectivDeterminers = new ArrayList<>();

		// Initialise cardinals
		for (int i = 0; i <= 1000; i++) {
			String numberInWords = Conversions.numberToWords(i).toLowerCase();
			adjectivDeterminers.add(new AdjectiveTypeDeterminer(numberInWords, AdjectiveTypes.CARDINAL));
			if (numberInWords.contains("-")) {
				adjectivDeterminers
						.add(new AdjectiveTypeDeterminer(numberInWords.replace("-", " "), AdjectiveTypes.CARDINAL));
			}
		}
		adjectivDeterminers.add(new AdjectiveTypeDeterminer("first", AdjectiveTypes.CARDINAL));
		adjectivDeterminers.add(new AdjectiveTypeDeterminer("last", AdjectiveTypes.CARDINAL));

		adjectivDeterminers.add(new AdjectiveTypeDeterminer("which", AdjectiveTypes.INTERROGATIVE));
		adjectivDeterminers.add(new AdjectiveTypeDeterminer("what", AdjectiveTypes.INTERROGATIVE));
		adjectivDeterminers.add(new AdjectiveTypeDeterminer("whose", AdjectiveTypes.INTERROGATIVE));

		adjectivDeterminers.addAll(getQuantitativeAdjectives());

		adjectivDeterminers.addAll(demonstrativeAdjectives());

		adjectivDeterminers.addAll(getEmotionalAdjectives());

		adjectiveTypeDeterminerRepository.saveAll(adjectivDeterminers);
		adjectiveTypeDeterminerRepository.flush();
		adjectivDeterminers.clear();

	}

	private List<AdjectiveTypeDeterminer> getQuantitativeAdjectives() {
		List<AdjectiveTypeDeterminer> quantitativeAdjectives = new ArrayList<>();

		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("many", AdjectiveTypes.QUANTITATIVE));
		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("most", AdjectiveTypes.QUANTITATIVE));
		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("all", AdjectiveTypes.QUANTITATIVE));
		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("any", AdjectiveTypes.QUANTITATIVE));
		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("half", AdjectiveTypes.QUANTITATIVE));
		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("much", AdjectiveTypes.QUANTITATIVE));
		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("sufficient", AdjectiveTypes.QUANTITATIVE));
		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("few", AdjectiveTypes.QUANTITATIVE));
		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("less", AdjectiveTypes.QUANTITATIVE));
		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("insufficient", AdjectiveTypes.QUANTITATIVE));
		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("heavily", AdjectiveTypes.QUANTITATIVE));
		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("light", AdjectiveTypes.QUANTITATIVE));
		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("some", AdjectiveTypes.QUANTITATIVE));
		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("little", AdjectiveTypes.QUANTITATIVE));
		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("no", AdjectiveTypes.QUANTITATIVE));
		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("empty", AdjectiveTypes.QUANTITATIVE));
		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("great", AdjectiveTypes.QUANTITATIVE));
		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("whole", AdjectiveTypes.QUANTITATIVE));
		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("enough", AdjectiveTypes.QUANTITATIVE));
		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("substantial", AdjectiveTypes.QUANTITATIVE));
		quantitativeAdjectives.add(new AdjectiveTypeDeterminer("single", AdjectiveTypes.QUANTITATIVE));
		return quantitativeAdjectives;
	}

	private List<AdjectiveTypeDeterminer> demonstrativeAdjectives() {
		List<AdjectiveTypeDeterminer> demonstrativeAdjectives = new ArrayList<>();
		demonstrativeAdjectives.add(new AdjectiveTypeDeterminer("this", AdjectiveTypes.DEMONSTRATIVE));
		demonstrativeAdjectives.add(new AdjectiveTypeDeterminer("that", AdjectiveTypes.DEMONSTRATIVE));
		demonstrativeAdjectives.add(new AdjectiveTypeDeterminer("yonder", AdjectiveTypes.DEMONSTRATIVE));
		demonstrativeAdjectives.add(new AdjectiveTypeDeterminer("yon", AdjectiveTypes.DEMONSTRATIVE));
		demonstrativeAdjectives.add(new AdjectiveTypeDeterminer("former", AdjectiveTypes.DEMONSTRATIVE));
		demonstrativeAdjectives.add(new AdjectiveTypeDeterminer("latter", AdjectiveTypes.DEMONSTRATIVE));
		demonstrativeAdjectives.add(new AdjectiveTypeDeterminer("these", AdjectiveTypes.DEMONSTRATIVE));
		demonstrativeAdjectives.add(new AdjectiveTypeDeterminer("those", AdjectiveTypes.DEMONSTRATIVE));
		return demonstrativeAdjectives;
	}

	private List<AdjectiveTypeDeterminer> getEmotionalAdjectives() {
		List<AdjectiveTypeDeterminer> emotionalAdjectiveWords = new ArrayList<>();
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("accepting", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("accommodating", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("afraid", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("aggressive", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("agitated", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("alarmed", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("amazed", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("amused", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("antagonistic", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("anxious", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("apathetic", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("apprehensive", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("arrogant", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("astonished", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("astounded", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("attentive blase", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("bold", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("bothered", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("brave", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("calm", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("capable", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("casual", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("charming", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("cheerful", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("cheery", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("churlish", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("collected", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("comfortable", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("competitive", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("composed", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("compulsive", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("concerned", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("confident", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("conflicted", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("conscientious", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("conservative", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("considerate", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("conspicuous", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("contemptible", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("content", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("convivial", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("cool", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("courageous", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("covetous", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("creative", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("critical", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("curious", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("cynical", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("dazzled", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("debilitated", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("defensive", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("dejected", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("delighted", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("demeaned", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("depressed", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("destructive", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("devious", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("devoted", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("dictatorial", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("diffident", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("disdainful", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("distracted", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("distraught", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("distressed", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("downcast", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("earnest", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("edgy", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("elated", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("empathetic", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("enthusiastic", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("euphoric", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("exhausted", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("expectant", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("explosive", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("exuberant", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("ferocious", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("fierce", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("flabbergasted", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("flexible", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("focused", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("forgiving", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("forlorn", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("frightened", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("furtive", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("gloomy", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("good", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("grateful", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("grouchy", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("guilty", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("happy", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("harassed", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("heroic", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("hesitant", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("hopeful", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("hostile", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("humble", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("humorous", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("hysterical", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("idealistic", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("ignorant", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("ill-tempered", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("impartial", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("impolite", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("imprudent", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("indifferent", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("infuriated", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("insightful", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("insulted", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("intense", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("intimidated", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("intolerant", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("irascible", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("jealous", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("jolly", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("jovial", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("joyful", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("jubilant", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("jumpy", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("kind", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("languid", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("liberal", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("loving", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("loyal", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("magical", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("magnificent", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("malevolent", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("malicious", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("mysterious", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("needy", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("negative", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("neglected", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("nervy", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("opinionated", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("panicky", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("passionate", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("patient", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("perturbed", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("petrified", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("petulant", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("placid", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("pleased", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("powerful", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("prejudicial", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("prideful", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("quarrelsome", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("queasy", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("quivering", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("rancorous", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("rational", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("reasonable", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("reckless", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("reflective", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("remorseful", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("repugnant", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("resilient", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("resolute", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("resourceful", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("respectful", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("responsible", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("responsive", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("restorative", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("reverent", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("rude", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("ruthless sad", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("safe", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("scared", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("scornful", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("seething", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("selfish", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("sensible", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("sensitive", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("serene", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("shaky", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("shivering", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("shocked", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("sickly", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("simple", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("sober", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("solemn", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("somber", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("sour", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("speechless", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("spooked", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("stern", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("successful", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("sullen", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("superior", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("supportive", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("surly", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("suspicious", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("sweet", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("sympathetic tactful", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("tenacious", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("tense", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("terrific", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("testy", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("thoughtful", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("thoughtless", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("timorous", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("tolerant", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("tranquil", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("treacherous", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("trembling", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("truthful", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("ultimate", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("uncivil", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("uncouth", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("uneasy", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("unethical", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("unfair", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("unique", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("unmannerly", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("unnerved", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("unrefined", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("unruffled", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("unsavory", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("unworthy", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("uplifting", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("upset", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("uptight", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("versatile", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("vicious", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("vigilant", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("vigorous", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("vile", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("villainous", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("virtuous", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("vivacious", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("volatile", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("vulnerable", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("warm", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("wary", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("waspish", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("weak", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("welcoming", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("wicked", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("wild", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("wise", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("wishy-washy", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("wistful", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("witty", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("woeful", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("wonderful", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("worrying", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("worthy", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("youthful", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("zany", AdjectiveTypes.EMOTIONAL));
		emotionalAdjectiveWords.add(new AdjectiveTypeDeterminer("zealous", AdjectiveTypes.EMOTIONAL));
		return emotionalAdjectiveWords;
	}

	@Override
	public void extract() {
		System.out.println("Extraction started");
		List<TwitterProfile> profiles = profileRepository.findAll();
		initialize();
		int count = 1;
		for (TwitterProfile profile : profiles) {
			System.out.println("processing profile " + (count++) + " " + profile.getTwitterId());
			savePosClassificationData(profile);
		}

	}

	@Async("extractTaskExecutor")
	public Future<TaskStatusBean> savePosClassificationData(TwitterProfile profile) {
		TaskStatusBean bean = new TaskStatusBean();
		try {
			List<AdjectiveSubClassification> adjectiveClassifications = new ArrayList<>();
			List<TweetAdjectives> adjectives = tweetPosClassificationRepository
					.findAdjectivesByProfile(profile.getTwitterId());
			for (TweetAdjectives adjective : adjectives) {
				String type = AdjectiveTypes.GENERAL.name();
				if (adjectiveTypes.containsKey(adjective.getWord().toLowerCase())) {
					type = adjectiveTypes.get(adjective.getWord().toLowerCase()).name();
				} else {
					String word = adjective.getWord().toLowerCase();
					boolean isFound=false;
					for(String emotionalWord:emotionalAdjectives) {
						if(word.contains(emotionalWord)) {
							type=AdjectiveTypes.EMOTIONAL.name();
							isFound=true;
							break;
						}
					}
					if ((!isFound)&&(adjective.getSubCategory().equalsIgnoreCase("Adjective, comparative")
							|| adjective.getSubCategory().equalsIgnoreCase("Adjective, superlative"))) {
						type = AdjectiveTypes.QUALITATIVE.name();
					}
				}
				adjectiveClassifications.add(new AdjectiveSubClassification(adjective.getTweetProfileId(),
						adjective.getTweetId(), adjective.getWord(), adjective.getWordPosition(), type));
			}
			saveService.saveAdjectiveClassifications(new ArrayList<>(adjectiveClassifications));
			adjectiveClassifications.clear();
		} catch (Exception ex) {
			bean.setError(true);
			bean.setMessage(ex.getMessage());

		}
		return new AsyncResult<TaskStatusBean>(bean);
	}

}
