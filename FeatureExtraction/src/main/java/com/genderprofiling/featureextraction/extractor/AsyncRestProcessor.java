package com.genderprofiling.featureextraction.extractor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Future;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.genderprofiling.featureextraction.bean.PolitenessRequestBean;
import com.genderprofiling.featureextraction.bean.PolitenessResponseBean;
import com.genderprofiling.featureextraction.bean.TaskStatusBean;
import com.genderprofiling.featureextraction.entity.TweetTone;
import com.genderprofiling.featureextraction.repository.TweetToneRepository;
import com.ibm.cloud.sdk.core.http.HttpConfigOptions;
import com.ibm.cloud.sdk.core.service.security.IamOptions;
import com.ibm.watson.tone_analyzer.v3.ToneAnalyzer;
import com.vdurmont.emoji.EmojiParser;

@Service
public class AsyncRestProcessor implements AsyncRestService {

	private static String POLITENESS_CHECKER_URL = "http://politeness.cornell.edu/score-politeness";
	private static String TONE_ANALYZER_ENDPOINT = "https://gateway-fra.watsonplatform.net/tone-analyzer/api";
	@Autowired(required = false)
	private RestTemplate restTemplate;

	private final Set<String> politeWords = new HashSet<>();
	@Autowired
	private TweetToneRepository toneRepository;

	private ToneAnalyzer toneAnalyzer;

	public AsyncRestProcessor() {
		IamOptions options = new IamOptions.Builder().apiKey("r3cPtQUjlkhSuDeUkpF2VjGsGDxiqojL5nl-HIfRq1t7").build();
		HttpConfigOptions configOptions = new HttpConfigOptions.Builder().disableSslVerification(true).build();
		toneAnalyzer = new ToneAnalyzer("2017-09-21", options);
		toneAnalyzer.configureClient(configOptions);
		// https://www.thespruce.com/polite-words-and-phrases-1216714
		// https://www.macmillandictionary.com/thesaurus-category/british/polite-words-and-expressions
		politeWords.add("hope ");
		politeWords.add(" hope ");
		politeWords.add(" hope");
		politeWords.add("seem ");
		politeWords.add(" seem");
		politeWords.add(" seem ");
		politeWords.add("respectfully ");
		politeWords.add(" respectfully");
		politeWords.add(" respectfully ");
		politeWords.add("perhaps ");
		politeWords.add(" perhaps");
		politeWords.add(" perhaps ");
		politeWords.add("delighted ");
		politeWords.add(" delighted");
		politeWords.add(" delighted ");
		politeWords.add("excuse me ");
		politeWords.add(" excuse me");
		politeWords.add(" excuse me ");
		politeWords.add("you are welcome");
		politeWords.add("you're welcome");
		politeWords.add("you're certainly welcome");
		politeWords.add("my pleasure");
		politeWords.add("i was happy to do it");
		politeWords.add(" thanks");
		politeWords.add("thanks ");
		politeWords.add(" thanks");
		politeWords.add(" thank you");
		politeWords.add("thank you ");
		politeWords.add(" may i ");
		politeWords.add(" may i");
		politeWords.add("pardon me");
		politeWords.add("i beg your pardon");
		politeWords.add("i am sorry");
		politeWords.add("i'm sorry");

	}

	@Async("extractTaskExecutor")
	@Override
	public Future<TaskStatusBean> checkPolitenessRestCall(List<PolitenessRequestBean> tweets) {
		int successCount = 0;
		int failureCount = 0;
		TaskStatusBean statusBean = new TaskStatusBean();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		String profileId = "";
		List<TweetTone> tweetTones = new ArrayList<>();
		for (PolitenessRequestBean request : tweets) {

			String reequestBody = "text=" + cleanTweet(request.getTweet());
			HttpEntity<String> httpRequest = new HttpEntity<>(reequestBody, headers);
			String label = "NA";
			int confidenceScore = 0;
			boolean isRequest = false;
			try {
				ResponseEntity<PolitenessResponseBean> response = restTemplate.postForEntity(POLITENESS_CHECKER_URL,
						httpRequest, PolitenessResponseBean.class);
				if (response.getStatusCode() == HttpStatus.OK) {
					PolitenessResponseBean politeness = response.getBody();
					label = politeness.getLabel();
					confidenceScore = getConfidenceScore(politeness.getConfidence());
					profileId = request.getTwitterId();
					isRequest = politeness.isIsrequest();
					successCount++;
				} else {
					failureCount++;
				}
			} catch (Exception e) {
				failureCount++;
			}
			tweetTones.add(
					new TweetTone(request.getTwitterId(), request.getTweetId(), label, confidenceScore, isRequest));

		}
		if (failureCount > 0) {
			statusBean.setError(true);
			statusBean.setMessage("failure count " + failureCount + "success count " + successCount);
			System.out.println("Failures for " + profileId + " count of failed " + failureCount);
		}
		toneRepository.saveAll(tweetTones);
		toneRepository.flush();
		return new AsyncResult<TaskStatusBean>(statusBean);

	}

	private int getConfidenceScore(String confidence) {
		int confidenceScore = 0;
		if (confidence != null) {
			confidenceScore = Integer.parseInt(confidence.split("%")[0]);
		}
		return confidenceScore;
	}

	private String cleanTweet(String tweet) {
		// remove urls , hashtags and emoticons
		tweet = EmojiParser.removeAllEmojis(tweet);
		return tweet.replaceAll("https?://\\S+\\s?", "").replaceAll("http?://\\S+\\s?", "")
				.replaceAll("#[A-Za-z0-9_]+", "").replaceAll("@[A-Za-z0-9_]+", "");
	}

	@Async("extractTaskExecutor")
	public Future<TaskStatusBean> analyzeTones(List<PolitenessRequestBean> tweets) {

		return null;
	}

	@Async("extractTaskExecutor")
	@Override
	public Future<TaskStatusBean> checkPoliteness(List<PolitenessRequestBean> tweets) {
		TaskStatusBean statusBean = new TaskStatusBean();
		List<TweetTone> tweetTones = new ArrayList<>();
		for (PolitenessRequestBean request : tweets) {
			boolean isPolite = false;
			String tweet = cleanTweet(request.getTweet());
			if (StringUtils.isNotEmpty(tweet)) {
				tweet = tweet.toLowerCase();
				for (String politeWord : politeWords) {
					if (tweet.contains(politeWord)) {
						isPolite = true;
						break;
					}
				}
			}
			tweetTones.add(
					new TweetTone(request.getTwitterId(), request.getTweetId(), isPolite ? "Polite" : "NP", 100, true));
		}
		toneRepository.saveAll(tweetTones);
		toneRepository.flush();
		return new AsyncResult<TaskStatusBean>(statusBean);

	}

}
