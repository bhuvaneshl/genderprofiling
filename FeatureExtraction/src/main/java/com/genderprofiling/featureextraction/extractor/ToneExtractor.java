package com.genderprofiling.featureextraction.extractor;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.genderprofiling.featureextraction.bean.PolitenessRequestBean;
import com.genderprofiling.featureextraction.entity.TweetData;
import com.genderprofiling.featureextraction.entity.TwitterProfile;
import com.genderprofiling.featureextraction.repository.TweetDataRepository;
import com.genderprofiling.featureextraction.repository.TwitterProfileRepository;


public class ToneExtractor implements SubFeaturesExtractor {
	
	@Autowired
	private AsyncRestService restProcessor;

	@Autowired
	private TwitterProfileRepository profileRepository;
	
	@Autowired
	private TweetDataRepository tweetDataRepository;

	
	@Override
	public void initializeData() {
	}

	@Override
	public void extract() {
		List<TwitterProfile> profiles = profileRepository.findAll();
		int idCount=0;
		for (TwitterProfile profile : profiles) {
			//get tweets
			List<TweetData> tweets=tweetDataRepository.findByTwitterId(profile.getTwitterId());
			List<PolitenessRequestBean> requests=new ArrayList<>();
			int count=0;
			for(TweetData tweetData:tweets) {
				requests.add(new PolitenessRequestBean(tweetData.getTwitterId(), tweetData.getTweetNumber(), tweetData.getTweet()));
				count++;
				if(count>=20) {
				restProcessor.checkPolitenessRestCall(new ArrayList<>(requests));
				count=0;
				requests.clear();
				
				}
			}
			if(!requests.isEmpty()) {
				restProcessor.checkPolitenessRestCall(new ArrayList<>(requests));
			}
			idCount++;
			System.out.println("extracted profiles "+idCount);
		}
	}

}
