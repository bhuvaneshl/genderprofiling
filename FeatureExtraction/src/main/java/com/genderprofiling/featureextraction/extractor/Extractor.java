/**
 * 
 */
package com.genderprofiling.featureextraction.extractor;

import java.util.List;

import com.genderprofiling.featureextraction.entity.BaseEntity;

public interface Extractor {

	public   List<? extends BaseEntity> extractTweet(String tweet, String twitterProfile, Integer tweetId) throws Exception;
	
	public Class<?> getReturnType();

	public void buildStatsSummaryIfRequired();

	public void doInitialCleanUpIfRequired();
	
	default	public boolean performExtraction() {
		return false;
	}
}
