package com.genderprofiling.featureextraction.extractor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.genderprofiling.featureextraction.entity.TagPosMapping;
import com.genderprofiling.featureextraction.entity.TweetPosClassification;
import com.genderprofiling.featureextraction.repository.ProfilePosClassificationSummaryRepository;
import com.genderprofiling.featureextraction.repository.TagPosMappingRepository;
import com.genderprofiling.featureextraction.repository.TweetPosClassificationRepository;

import edu.stanford.nlp.tagger.maxent.MaxentTagger;

@Transactional
@Service
public class PosExtractor implements Extractor {

	@Autowired
	private TweetPosClassificationRepository posClassificationRepository;

	@Autowired
	private TagPosMappingRepository posMappingRepository;

	@Autowired
	private ProfilePosClassificationSummaryRepository classificationSummaryRepository;

	private Map<String, TagPosMapping> tagPosMapper;

	private MaxentTagger maxentTagger;

	public PosExtractor() {
		maxentTagger = new MaxentTagger("models/gate-EN-twitter.model");
		// maxentTagger = new MaxentTagger("models/english-left3words-distsim.tagger");

		tagPosMapper = new HashMap<>();

	}

	private Map<String, TagPosMapping> getPosMapper() {
		if (tagPosMapper == null || tagPosMapper.isEmpty()) {
			tagPosMapper = posMappingRepository.findAll().stream()
					.collect(Collectors.toMap(TagPosMapping::getTag, obj -> obj));

		}
		return tagPosMapper;
	}

	@Override
	public List<TweetPosClassification> extractTweet(String tweet, String twitterProfile, Integer tweetId)
			throws Exception {
		List<TweetPosClassification> listToSave = new ArrayList<>();
		String taggedData = maxentTagger.tagString(tweet);
		if (taggedData != null && !taggedData.isEmpty()) {
			String[] wordsAndTags = taggedData.split("\\s+");
			if (wordsAndTags.length > 0) {
				for (int i = 0; i < wordsAndTags.length; i++) {
					String wordAndTag = wordsAndTags[i];
					String word = wordAndTag.split("_")[0];
					String tag = wordAndTag.split("_")[1];
					if (getPosMapper().containsKey(tag)) {
						listToSave.add(new TweetPosClassification(twitterProfile, tweetId, word, i,
								getPosMapper().get(tag).getId()));
					} else {
						if(word.startsWith("@")) {
							listToSave.add(new TweetPosClassification(twitterProfile, tweetId, word, i,
									getPosMapper().get("USR").getId()));
						}else {
						listToSave.add(new TweetPosClassification(twitterProfile, tweetId, word, i,
								getPosMapper().get("UnCategorized").getId()));
						}
					}

				}
			}

		}
		return listToSave;
	}

	@Override
	public void buildStatsSummaryIfRequired() {
		classificationSummaryRepository.deleteAll();
		classificationSummaryRepository.insertIntoSummaryTable();
	}

	@Override
	public void doInitialCleanUpIfRequired() {
		classificationSummaryRepository.truncate();
		posClassificationRepository.deleteAll();

	}

	@Override
	public Class<?> getReturnType() {
		// TODO Auto-generated method stub
		return TweetPosClassification.class;
	}
	
}
