package com.genderprofiling.featureextraction.extractor;

public interface SubFeaturesExtractor {

	
	public void initializeData();
	public void extract(); 
}
