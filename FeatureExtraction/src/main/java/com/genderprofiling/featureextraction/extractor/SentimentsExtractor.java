package com.genderprofiling.featureextraction.extractor;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.genderprofiling.featureextraction.entity.TweetSentiment;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations.SentimentAnnotatedTree;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;


public class SentimentsExtractor implements Extractor {
	private StanfordCoreNLP pipeline;

	public SentimentsExtractor() {
		Properties props = new Properties();
		props.setProperty("annotators", "tokenize, ssplit, parse, sentiment");
		props.setProperty("ssplit.eolonly", "true");
		pipeline = new StanfordCoreNLP(props);
	}

	@Override
	public List<TweetSentiment> extractTweet(String tweet, String twitterProfile, Integer tweetId) throws Exception {
		List<TweetSentiment> stats = new ArrayList<>();
		int sentimentScore = 0;
		String sentimentType = null;
		if (tweet != null && tweet.length() > 0) {
			int longest = 0;
			Annotation annotation = pipeline.process(tweet);
			for (CoreMap sentence : annotation.get(CoreAnnotations.SentencesAnnotation.class)) {
				Tree tree = sentence.get(SentimentAnnotatedTree.class);
				int sentiment = RNNCoreAnnotations.getPredictedClass(tree);
				String partText = sentence.toString();
				sentimentType = sentence.get(SentimentCoreAnnotations.SentimentClass.class);
				if (partText.length() > longest) {
					sentimentScore = sentiment;
					longest = partText.length();
				}
			}
		}
		stats.add(new TweetSentiment(twitterProfile, tweetId, sentimentScore, sentimentType));
		return stats;

	}

	@Override
	public void buildStatsSummaryIfRequired() {

	}

	@Override
	public void doInitialCleanUpIfRequired() {

	}

	@Override
	public Class<?> getReturnType() {
		return TweetSentiment.class;
	}

}
