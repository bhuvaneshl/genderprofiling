package com.genderprofiling.featureextraction.extractor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.genderprofiling.featureextraction.Constants;
import com.genderprofiling.featureextraction.entity.BadWordsStats;

@Service
public class BadWordsExtractor implements Extractor {
	private Set<String> allBadWords = new HashSet<>();

	@Override
	public List<BadWordsStats> extractTweet(String tweet, String twitterProfile, Integer tweetId) throws Exception {
		List<String> badWordsList = badWordsFound(tweet);
		List<BadWordsStats> stats = new ArrayList<>();
		for (String badWord : badWordsList) {
			stats.add(new BadWordsStats(twitterProfile, tweetId, badWord));
		}
		return stats;

	}

	@Override
	public void buildStatsSummaryIfRequired() {

	}

	@Override
	public void doInitialCleanUpIfRequired() {
		// https://www.cs.cmu.edu/~biglou/resources/
		try {
			allBadWords = Files.readAllLines(Paths.get(Constants.getBadWordsFile())).stream().map(s -> s.toLowerCase())
					.collect(Collectors.toSet());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private List<String> badWordsFound(String tweet) {
		if (tweet == null) {
			return new ArrayList<>();
		}

		// remove leetspeak
		tweet = tweet.replaceAll("1", "i");
		tweet = tweet.replaceAll("!", "i");
		tweet = tweet.replaceAll("3", "e");
		tweet = tweet.replaceAll("4", "a");
		tweet = tweet.replaceAll("@", "a");
		tweet = tweet.replaceAll("5", "s");
		tweet = tweet.replaceAll("7", "t");
		tweet = tweet.replaceAll("0", "o");
		tweet = tweet.replaceAll("9", "g");
		tweet=tweet.toLowerCase();
		ArrayList<String> badWords = new ArrayList<>();
		for (String badWord : allBadWords) {
			String regex = ".*\\b" + Pattern.quote(badWord) + "\\b.*"; 
			if (tweet.matches(regex)) {
				badWords.add(badWord);
			}
		}
		return badWords;

	}

	@Override
	public Class<?> getReturnType() {
		return BadWordsStats.class;
	}

	@Override
	public boolean performExtraction() {
		return true;
	}

}
