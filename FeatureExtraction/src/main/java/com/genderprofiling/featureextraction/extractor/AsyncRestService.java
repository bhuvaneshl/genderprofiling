package com.genderprofiling.featureextraction.extractor;

import java.util.List;
import java.util.concurrent.Future;

import com.genderprofiling.featureextraction.bean.PolitenessRequestBean;
import com.genderprofiling.featureextraction.bean.TaskStatusBean;

public interface AsyncRestService {

	public Future<TaskStatusBean> checkPolitenessRestCall(List<PolitenessRequestBean> tweets);
	
	public Future<TaskStatusBean> checkPoliteness(List<PolitenessRequestBean> tweets);

}
