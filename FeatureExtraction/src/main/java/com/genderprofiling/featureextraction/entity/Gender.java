package com.genderprofiling.featureextraction.entity;

public enum Gender {

	MALE,
	FEMALE
}
