package com.genderprofiling.featureextraction.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class AdjectiveSubClassification extends BaseEntity {

	
	protected String twitterProfileId;

	protected Integer tweetId;
	
	protected Integer wordPosition;
	
	private String adjectiveType;
	
	private String word;

	public AdjectiveSubClassification(String twitterProfileId, Integer tweetId, String word, Integer wordPosition,String adjectiveType) {
		this.twitterProfileId = twitterProfileId;
		this.tweetId = tweetId;
		this.word = word;
		this.wordPosition = wordPosition;
		this.adjectiveType = adjectiveType;
	}

	public AdjectiveSubClassification() {
	}

	public String getAdjectiveType() {
		return adjectiveType;
	}

	public String getWord() {
		return word;
	}
	
	
	
	

}
