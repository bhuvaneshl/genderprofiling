package com.genderprofiling.featureextraction.entity;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;

import com.genderprofiling.featureextraction.bean.ProfilePosCategorySummary;

@Entity
@Table(name = "profile_pos_classification_summary")
@SqlResultSetMappings({
		@SqlResultSetMapping(name = "mapProfileCategorySummary", classes = {
				@ConstructorResult(targetClass = ProfilePosCategorySummary.class, columns = {
						@ColumnResult(name = "profileId", type = String.class),
						@ColumnResult(name = "category", type = String.class),
						@ColumnResult(name = "totalcount", type = Integer.class) }) }),
		@SqlResultSetMapping(name = "mapCategorySummary", classes = {
				@ConstructorResult(targetClass = ProfilePosCategorySummary.class, columns = {
						@ColumnResult(name = "category", type = String.class),
						@ColumnResult(name = "totalcount", type = Integer.class) }) }) })

@NamedNativeQueries({
		@NamedNativeQuery(name = "ProfilePosClassificationSummary.getProfileCategorySummary", query = "  select twitter_profile_id as profileId,category as category,sum(total) as totalcount from "
				+ "profile_pos_classification_summary group by  twitter_profile_id,category", resultSetMapping = "mapProfileCategorySummary"),

		@NamedNativeQuery(name = "ProfilePosClassificationSummary.getCategorySummary", query = "select category as category,sum(total) as totalcount "
				+ "from profile_pos_classification_summary group by category", resultSetMapping = "mapCategorySummary"),
		@NamedNativeQuery(name = "ProfilePosClassificationSummary.getWordsCountByProfile", query = "select twitter_profile_id as profileId,' ' as category,count(word) as totalCount from "
				+ "tweet_pos_classification group by twitter_profile_id order by twitter_profile_id asc;", resultSetMapping = "mapProfileCategorySummary") })

public class ProfilePosClassificationSummary extends BaseEntity {

	private String twitterProfileId;

	private String subCategory;

	private String category;

	private int total;

	public String getTwitterProfileId() {
		return twitterProfileId;
	}

	public void setTwitterProfileId(String twitterProfileId) {
		this.twitterProfileId = twitterProfileId;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

}
