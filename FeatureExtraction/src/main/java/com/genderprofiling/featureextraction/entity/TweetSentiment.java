package com.genderprofiling.featureextraction.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class TweetSentiment extends BaseEntity {

	private String twitterProfileId;

	private Integer tweetId;

	private Integer sentimentScore;
	
	private String sentiment;
	
	

	public TweetSentiment(String twitterProfileId, Integer tweetId, Integer sentimentScore,String sentiment) {
		super();
		this.twitterProfileId = twitterProfileId;
		this.tweetId = tweetId;
		this.sentimentScore = sentimentScore;
		this.sentiment=sentiment;
	}

	public TweetSentiment() {
		super();
	}

	public String getTwitterProfileId() {
		return twitterProfileId;
	}

	public void setTwitterProfileId(String twitterProfileId) {
		this.twitterProfileId = twitterProfileId;
	}


	public Integer getTweetId() {
		return tweetId;
	}

	public void setTweetId(Integer tweetId) {
		this.tweetId = tweetId;
	}

	public Integer getSentimentScore() {
		return sentimentScore;
	}

	public void setSentimentScore(Integer sentimentScore) {
		this.sentimentScore = sentimentScore;
	}

	public String getSentiment() {
		return sentiment;
	}

	public void setSentiment(String sentiment) {
		this.sentiment = sentiment;
	}
	
	

}
