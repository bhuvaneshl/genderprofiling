package com.genderprofiling.featureextraction.entity;

import javax.persistence.Entity;

@Entity


public class TagPosMapping extends BaseEntity {

	private String tag;

	private String category;

	private String subCategory;

	@SuppressWarnings("unused")
	private TagPosMapping() {
		super();
	}

	public TagPosMapping(String category, String tag, String subCategory) {
		this.tag = tag;
		this.category = category;
		this.subCategory = subCategory;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

}
