package com.genderprofiling.featureextraction.entity;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;

import com.genderprofiling.featureextraction.bean.ProfileStatBean;

@Entity
@Table
@SqlResultSetMappings({ @SqlResultSetMapping(name = "mapBadWordsySummary", classes = {
		@ConstructorResult(targetClass = ProfileStatBean.class, columns = {
				@ColumnResult(name = "profileId", type = String.class),
				@ColumnResult(name = "gender", type = String.class),
				@ColumnResult(name = "count", type = Integer.class),
				@ColumnResult(name = "matchedData", type = String.class) }) }),

})
@NamedNativeQueries({
		@NamedNativeQuery(name = "BadWordsStats.getBadWordsSummary", query = "select 							"
				+ "	profile.twitter_id as profileId, 							"
				+ "	profile.gender as gender, 									"
				+ "	count(bad.word) as count, 									"
				+ "	array_agg(bad.word) as matchedData 							"
				+ "from 														"
				+ " twitter_profile profile left join   						"
				+ " 	bad_words_stats bad on 									"
				+ "	profile.twitter_id = bad.twitter_profile_id 				"
				+ "	group by 1,2 order by 1 asc   								"
				+ " ", resultSetMapping = "mapBadWordsySummary")

})

public class BadWordsStats extends BaseEntity {

	private String twitterProfileId;

	private Integer tweetId;

	private String word;

	public BadWordsStats(String twitterProfileId, Integer tweetId, String word) {
		super();
		this.twitterProfileId = twitterProfileId;
		this.tweetId = tweetId;
		this.word = word;
	}

	public BadWordsStats() {
		super();
	}

	public String getTwitterProfileId() {
		return twitterProfileId;
	}

	public void setTwitterProfileId(String twitterProfileId) {
		this.twitterProfileId = twitterProfileId;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public Integer getTweetId() {
		return tweetId;
	}

	public void setTweetId(Integer tweetId) {
		this.tweetId = tweetId;
	}

}
