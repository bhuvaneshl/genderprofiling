package com.genderprofiling.featureextraction.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
public class TwitterProfile extends BaseEntity {

	private String twitterId;

	@Enumerated(EnumType.STRING)
	private Gender gender;

	private String country;

	public String getTwitterId() {
		return twitterId;
	}

	public void setTwitterId(String twitterId) {
		this.twitterId = twitterId;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public TwitterProfile(String twitterId, String gender, String country) {
		super();
		this.twitterId = twitterId;
		this.gender = Gender.valueOf(gender.toUpperCase());
		this.country = country;
	}

	public TwitterProfile() {
		super();
	}
	

}
