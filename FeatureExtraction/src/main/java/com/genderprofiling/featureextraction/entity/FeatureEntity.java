package com.genderprofiling.featureextraction.entity;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class FeatureEntity extends BaseEntity{

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "twitter_profile_id", insertable = false, updatable = false, referencedColumnName = "twitterId")
	protected TwitterProfile twitterProfile;

	@Column(name = "twitter_profile_id", insertable = true)
	protected String twitterProfileId;

	protected Integer tweetId;
	
	protected Integer wordPosition;
	

	public String getTwitterProfileId() {
		return twitterProfileId;
	}

	public TwitterProfile getTwitterProfile() {
		return twitterProfile;
	}

	public void setTwitterProfileId(String twitterProfileId) {
		this.twitterProfileId = twitterProfileId;
	}

	public Integer getTweetId() {
		return tweetId;
	}

	public void setTweetId(Integer tweetId) {
		this.tweetId = tweetId;
	}

	public Integer getWordPosition() {
		return wordPosition;
	}

	public void setWordPosition(Integer wordPosition) {
		this.wordPosition = wordPosition;
	}
	
	
	
}
