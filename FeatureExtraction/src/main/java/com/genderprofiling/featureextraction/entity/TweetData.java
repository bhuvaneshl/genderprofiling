package com.genderprofiling.featureextraction.entity;

import javax.persistence.Entity;

@Entity
public class TweetData extends BaseEntity {

	private String twitterId;
	
	private Integer tweetNumber;
	
	private String tweet;

	public String getTwitterId() {
		return twitterId;
	}
	
	

	
	public TweetData() {
	}




	public TweetData(String twitterId, Integer tweetNumber, String tweet) {
		super();
		this.twitterId = twitterId;
		this.tweetNumber = tweetNumber;
		this.tweet = tweet;
	}


	public void setTwitterId(String twitterId) {
		this.twitterId = twitterId;
	}

	public Integer getTweetNumber() {
		return tweetNumber;
	}

	public void setTweetNumber(Integer tweetNumber) {
		this.tweetNumber = tweetNumber;
	}

	public String getTweet() {
		return tweet;
	}

	public void setTweet(String tweet) {
		this.tweet = tweet;
	}

		
	

}
