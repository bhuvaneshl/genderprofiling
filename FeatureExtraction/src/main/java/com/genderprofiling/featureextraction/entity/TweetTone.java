package com.genderprofiling.featureextraction.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class TweetTone extends BaseEntity {


	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "twitter_profile_id", insertable = false, updatable = false, referencedColumnName = "twitterId")
	protected TwitterProfile twitterProfile;

	@Column(name = "twitter_profile_id", insertable = true)
	private String twitterProfileId;

	private boolean isRequest;
	
	private Integer tweetId;
	
	private String label;
	
	private  int confidenceScore;
	
	

	public TweetTone( String twitterProfileId, Integer tweetId, String label,
			int confidenceScore,boolean isRequest) {
		super();
		this.twitterProfileId = twitterProfileId;
		this.tweetId = tweetId;
		this.label = label;
		this.confidenceScore = confidenceScore;
		this.isRequest=isRequest;
	}

	public TwitterProfile getTwitterProfile() {
		return twitterProfile;
	}

	public String getTwitterProfileId() {
		return twitterProfileId;
	}

	public Integer getTweetId() {
		return tweetId;
	}

	public String getLabel() {
		return label;
	}

	public int getConfidenceScore() {
		return confidenceScore;
	}

	public boolean isRequest() {
		return isRequest;
	}
	
	
	
}
