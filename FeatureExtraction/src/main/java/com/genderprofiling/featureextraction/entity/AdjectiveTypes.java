package com.genderprofiling.featureextraction.entity;

public enum AdjectiveTypes {
	QUALITATIVE,
	QUANTITATIVE,
	CARDINAL,
	EMOTIONAL, //https://myvocabulary.com/word-list/adjectives-of-emotions-vocabulary/
	DEMONSTRATIVE,
	TECHNICAL,
	INTERROGATIVE,
	GENERAL
}
