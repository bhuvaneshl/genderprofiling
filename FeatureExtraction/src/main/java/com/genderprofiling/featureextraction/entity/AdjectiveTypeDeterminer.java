package com.genderprofiling.featureextraction.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
public class AdjectiveTypeDeterminer extends BaseEntity {

	private String word;
	
	private String type;

	
	public AdjectiveTypeDeterminer() {
	}

	public AdjectiveTypeDeterminer(String word, AdjectiveTypes type) {
		super();
		this.word = word;
		this.type = type.name();
	}

	public String getWord() {
		return word;
	}

	public String getType() {
		return type;
	}

	
	
}
