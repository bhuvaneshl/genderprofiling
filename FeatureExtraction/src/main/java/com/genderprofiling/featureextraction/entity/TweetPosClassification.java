package com.genderprofiling.featureextraction.entity;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;

import org.hibernate.annotations.NamedNativeQueries;
import org.hibernate.annotations.NamedNativeQuery;

import com.genderprofiling.featureextraction.bean.ProfilePosCategorySummary;
import com.genderprofiling.featureextraction.bean.ProfileStatBean;

@Entity
@Table(name = "tweet_pos_classification")
@SqlResultSetMappings({
		@SqlResultSetMapping(name = "mapWordCount", classes = {
				@ConstructorResult(targetClass = ProfilePosCategorySummary.class, columns = {
						@ColumnResult(name = "twitter_profile_id", type = String.class),
						@ColumnResult(name = "category", type = String.class),
						@ColumnResult(name = "totalcount", type = Integer.class) }) }),
		@SqlResultSetMapping(name = "mapSummary", classes = {
				@ConstructorResult(targetClass = ProfileStatBean.class, columns = {
						@ColumnResult(name = "profileId", type = String.class),
						@ColumnResult(name = "gender", type = String.class),
						@ColumnResult(name = "count", type = Integer.class),
						@ColumnResult(name = "matchedData", type = String.class) }) }) })

@NamedNativeQueries({
		@NamedNativeQuery(name = "TweetPosClassification.getWordsCount", query = "	 select  twitter_profile_id,' ' as category,count(word) totalcount from tweet_pos_classification  "
				+ " group by twitter_profile_id ", resultSetMapping = "mapWordCount"),
		@NamedNativeQuery(name = "TweetPosClassification.getQualitativeAdjAdverbsSummary", query = "	 select  twitter_profile_id,' ' as category,count(word) totalcount from tweet_pos_classification  "
				+ " group by twitter_profile_id ", resultSetMapping = "mapWordCount"),
		@NamedNativeQuery(name = "TweetPosClassification.getVocabularyRichnessByProfile", query = "select								 "
				+ "	tweet.twitter_profile_id as profileId, 								"
				+ "	map.sub_category as gender, 										"
				+ "	count(tweet.word) as count, 										"
				+ "	array_agg(distinct lower(tweet.word)) as matchedData 				"
				+ "from 																"
				+ "	tweet_pos_classification tweet 										"
				+ "join tag_pos_mapping map on 											"
				+ "	tweet.tag_id = map.id 												"
				+ "where 																"
				+ "	map.sub_category in('Adjective', 									"
				+ "	'Adjective, comparative', 											"
				+ "	'Adjective, superlative', 											"
				+ "	'Adverb', 															"
				+ "	'Adverb, comparative', 												"
				+ "	'Adverb, superlative') 											"
				+ "group by 1,	2 order by	1	", resultSetMapping = "mapSummary")

})

public class TweetPosClassification extends FeatureEntity {

	private String word;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tag_id", insertable = false, updatable = false)
	private TagPosMapping tagPosMapping;

	@Column(name = "tag_id", insertable = true)
	private Long tagId;

	public Long getTagId() {
		return tagId;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public TagPosMapping getTagPosMapping() {
		return tagPosMapping;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	public TweetPosClassification() {
	}

	public TweetPosClassification(String twitterProfileId, Integer tweetId, String word, Integer wordPosition,
			Long tagId) {
		this.twitterProfileId = twitterProfileId;
		this.tweetId = tweetId;
		this.word = word;
		this.wordPosition = wordPosition;
		this.tagId = tagId;
	}

}
