package com.genderprofiling.featureextraction.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.genderprofiling.featureextraction.entity.TweetTone;

@Repository
public interface TweetToneRepository extends JpaRepository<TweetTone, Long> {

}
