package com.genderprofiling.featureextraction.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.genderprofiling.featureextraction.entity.AdjectiveSubClassification;

@Repository
public interface AdjectiveSubClassificationRepository extends JpaRepository<AdjectiveSubClassification, Long> {

	
}
