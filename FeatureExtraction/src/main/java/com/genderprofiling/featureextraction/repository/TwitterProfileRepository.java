package com.genderprofiling.featureextraction.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.genderprofiling.featureextraction.entity.TwitterProfile;

public interface TwitterProfileRepository extends JpaRepository<TwitterProfile, Long> {

	List<TwitterProfile> findAllByOrderByTwitterIdAsc();
}
