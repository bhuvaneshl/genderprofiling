package com.genderprofiling.featureextraction.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.genderprofiling.featureextraction.entity.TweetSentiment;

@Repository
public interface TweetSentimentRepository extends JpaRepository<TweetSentiment, Long> {

	
}
