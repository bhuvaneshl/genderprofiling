package com.genderprofiling.featureextraction.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.genderprofiling.featureextraction.bean.ProfileStatBean;
import com.genderprofiling.featureextraction.entity.BadWordsStats;

@Repository
public interface BadWordsStatsRepository extends JpaRepository<BadWordsStats, Long> {

	@Query(nativeQuery = true)
	public List<ProfileStatBean> getBadWordsSummary();
}
