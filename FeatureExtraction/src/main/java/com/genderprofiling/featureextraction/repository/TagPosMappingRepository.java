package com.genderprofiling.featureextraction.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.genderprofiling.featureextraction.entity.TagPosMapping;

@Repository
public interface TagPosMappingRepository extends JpaRepository<TagPosMapping, Long> {
	
	
}
