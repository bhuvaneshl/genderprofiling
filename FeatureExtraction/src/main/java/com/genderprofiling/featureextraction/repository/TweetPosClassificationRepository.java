package com.genderprofiling.featureextraction.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.genderprofiling.featureextraction.bean.ProfilePosCategorySummary;
import com.genderprofiling.featureextraction.bean.ProfileStatBean;
import com.genderprofiling.featureextraction.bean.TweetAdjectives;
import com.genderprofiling.featureextraction.entity.TweetPosClassification;

@Repository
public interface TweetPosClassificationRepository extends JpaRepository<TweetPosClassification, Long> {

	@Query(nativeQuery = true)
	public List<ProfilePosCategorySummary> getWordsCount();

	@Query(value = "truncate table tweet_pos_classification", nativeQuery = true)
	public void truncate();

	@Query(nativeQuery = true)
	public List<ProfileStatBean> getVocabularyRichnessByProfile();

	@Query("Select new  com.genderprofiling.featureextraction.bean.TweetAdjectives(pos.twitterProfileId,pos.tweetId,pos.wordPosition,pos.tagPosMapping.subCategory,pos.word) from TweetPosClassification pos where pos.tagPosMapping.category='Adjective' and pos.twitterProfileId=:twitterProfileId")
	public List<TweetAdjectives> findAdjectivesByProfile(@Param("twitterProfileId")String profileId);
}
