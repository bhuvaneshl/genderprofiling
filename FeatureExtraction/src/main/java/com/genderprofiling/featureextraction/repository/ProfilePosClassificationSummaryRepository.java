package com.genderprofiling.featureextraction.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.genderprofiling.featureextraction.bean.ProfilePosCategorySummary;
import com.genderprofiling.featureextraction.entity.ProfilePosClassificationSummary;

@Repository
public interface ProfilePosClassificationSummaryRepository
		extends JpaRepository<ProfilePosClassificationSummary, String> {

	@Modifying
	@Query(value = "insert into profile_pos_classification_summary (twitter_profile_id,category,sub_category,total) "
			+ " ( select cl.twitter_profile_id,pm.category,pm.sub_category,count(cl.word) from tweet_pos_classification cl,twitter_profile tp,tag_pos_mapping pm "
			+ "  where " + "  	cl.twitter_profile_id=tp.twitter_id and cl.tag_id=pm.id "
			+ "  	group by cl.twitter_profile_id,pm.category,pm.sub_category) ", nativeQuery = true)
	public void insertIntoSummaryTable();

	@Modifying
	@Query(value = "truncate table profile_pos_classification_summary", nativeQuery = true)
	public void truncate();

	@Query(nativeQuery = true)
	public List<ProfilePosCategorySummary> getProfileCategorySummary();

	@Query(nativeQuery = true)
	public List<ProfilePosCategorySummary> getCategorySummary();

	@Query(nativeQuery = true)
	public List<ProfilePosCategorySummary> getWordsCountByProfile();

	@Query(value = " select distinct pm.category from tweet_pos_classification cl,twitter_profile tp,tag_pos_mapping pm where "
			+ "  	cl.twitter_profile_id=tp.twitter_id and cl.tag_id=pm.id ", nativeQuery = true)
	public List<String> getPOSCategories();

}
