package com.genderprofiling.featureextraction.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.genderprofiling.featureextraction.entity.AdjectiveTypeDeterminer;

@Repository
public interface AdjectiveTypeDeterminerRepository extends JpaRepository<AdjectiveTypeDeterminer, Long> {

	
}
