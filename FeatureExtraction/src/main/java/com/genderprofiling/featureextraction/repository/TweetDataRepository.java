package com.genderprofiling.featureextraction.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.genderprofiling.featureextraction.entity.TweetData;

@Repository
public interface TweetDataRepository extends JpaRepository<TweetData,Long> {
	
	List<TweetData> findByTwitterId(String twitterId);
}
