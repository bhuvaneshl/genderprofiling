package com.genderprofiling.featureextraction.utils;

import java.io.IOException;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;



public final class ZipFileHandler {

	/**
	 * created to avoid class Instantiation
	 */
	private ZipFileHandler() {

	}

	public static void  unzip(String source,String destination) throws IOException {
		try {
	         ZipFile zipFile = new ZipFile(source);
	         zipFile.extractAll(destination);
	    } catch (ZipException e) {
	        e.printStackTrace();
	    }

	}
}
