package com.genderprofiling.featureextraction.utils;

import pl.allegro.finance.tradukisto.ValueConverters;

public class Conversions {
	private static ValueConverters converter = ValueConverters.ENGLISH_INTEGER;
	
	public static String numberToWords(Integer number) {
		return converter.asWords(number);
	}
	
}
