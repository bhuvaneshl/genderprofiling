package com.genderprofiling.featureextraction.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.jsoup.Jsoup;

import com.genderprofiling.featureextraction.Constants;
import com.genderprofiling.featureextraction.ExcelWriter;
import com.genderprofiling.featureextraction.xml.Author;
import com.genderprofiling.featureextraction.xml.Documents;

class TempCsvConverter {

	public static void main(String[] args) throws IOException {
		Map<String, String> profileGenderMap = new HashMap<>();
		ExcelWriter excelWriter = new ExcelWriter();
		Files.readAllLines(Paths.get(Constants.getDataDirectoryTruthFile())).stream().forEach(line -> {
			String s[] = line.split(":::");
			profileGenderMap.put(s[0], s[1]);
		});
		excelWriter.createSheet("tweets");
		List<String> headers = new ArrayList<>();
		Collections.sort(headers);
		headers.add(0, "Person");
		headers.add(1, "Gender");
		for (int ind = 1; ind <= 50; ind++) {
			headers.add(1 + ind, "Tweet_" + ind);
		}
		excelWriter.addHeaderRow(headers);
		List<String> truthData = new ArrayList<>();
		Files.list(Paths.get(Constants.getDataDirectory())).forEach(filePath -> {
			int invalidTweets = 0;
			if (!filePath.endsWith("truth.txt")) {
				Author author = new Author();
				author.setDocuments(new Documents());
				author.getDocuments().getDocument();
				Map<String, Object> rowMap = new HashMap<>();
				String twitterProfileId = filePath.getFileName().toString().replace(".xml", "");
				String gender = profileGenderMap.get(twitterProfileId);
				rowMap.put("Person", twitterProfileId);
				rowMap.put("Gender", gender);
				int tweetId = 1;
				for (String tweet : readXmlFileAndGetTweetsAsList(filePath)) {
					if (tweet != null && !tweet.trim().isEmpty()) {

						String cleanedTweet = Jsoup.parse(tweet).text();
						cleanedTweet = cleanedTweet.replace("http://", " http://").replace("https://", " https://")
								.replace("&amp;", "");
						if (cleanedTweet.length() > 150) {
							invalidTweets++;
							continue;
						}
						rowMap.put("Tweet_" + tweetId, cleanedTweet);
						author.getDocuments().getDocument().add(cleanedTweet);
						tweetId++;
					}
					if (tweetId > 50) {
						break;
					}
				}
				System.out.println("invalid tweets:" + invalidTweets);
				if (tweetId >= 50) {
					truthData.add(
							new StringBuilder().append(twitterProfileId).append(":::").append(gender).append(":::").append("NA").toString());
					excelWriter.addRow(rowMap);
					try {
						writeTweetsToFile(author, Files
								.createFile(Paths.get(Constants.getOutputFileDirectory(), twitterProfileId)).toFile());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});

		excelWriter.writeToFile("tweets.xlsx");
		Files.write(Paths.get(Constants.getOutputFileDirectory(), "truth_.txt"), truthData,
				StandardOpenOption.CREATE);
	}

	private static List<String> readXmlFileAndGetTweetsAsList(Path filePath) {
		List<String> list = new ArrayList<>();
		JAXBContext jc;
		try {
			jc = JAXBContext.newInstance(Author.class);
			final Unmarshaller unmarshaller = jc.createUnmarshaller();
			Author author = (Author) unmarshaller.unmarshal(filePath.toFile());
			if (author != null && author.getDocuments() != null && author.getDocuments().getDocument() != null) {
				list = author.getDocuments().getDocument();
			}
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return list;
	}

	private static void writeTweetsToFile(Author author, File file) {
		JAXBContext jc;
		try {
			jc = JAXBContext.newInstance(Author.class);
			final Marshaller unmarshaller = jc.createMarshaller();
			unmarshaller.marshal(author, file);

		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
