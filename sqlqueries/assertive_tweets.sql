---assertive tweets data
select
	tweet_data.twitter_id,
	tweet_data.tweet,
	temp1.categories
from
	tweet_data tweet_data
join (
		select tweet.twitter_profile_id as profile_id,
		tweet.tweet_id as tweet_id ,
		array_agg(tweet.word_position) as word_positions,
		array_to_string(array_agg(tag.sub_category),',') as categories
	from
		tweet_pos_classification tweet
	join tag_pos_mapping tag on
		tag.id = tweet.tag_id
	where
		tag.category in('Modal','Noun') 

	group by
		1,
		2)  temp1 on
	tweet_data.twitter_id=temp1.profile_id and tweet_data.tweet_number=temp1.tweet_id
	where lower(temp1.categories) like '%noun%modal%'
    and (lower(tweet_data.tweet) like '%must%'
     or lower(tweet_data.tweet) like '%should%') 
      ;
	  
--assertive tweets summary
create	table if not exists assertive_tweets (twitter_id varchar(300),total_count int);
truncate table assertive_tweets;

insert into assertive_tweets (
			select tweet_data.twitter_id,
			count(distinct tweet_data.tweet)
		from
			tweet_data tweet_data
		join (
			select
				tweet.twitter_profile_id as profile_id,
				tweet.tweet_id as tweet_id ,
				array_agg(tweet.word_position) as word_positions,
				array_to_string(array_agg(tag.sub_category), ',') as categories
			from
				tweet_pos_classification tweet
			join tag_pos_mapping tag on
				tag.id = tweet.tag_id
			where
				tag.category in('Modal',
				'Noun')
			group by
				1,
				2) temp1 on
			tweet_data.twitter_id = temp1.profile_id
			and tweet_data.tweet_number = temp1.tweet_id
		where
			lower(temp1.categories) like '%noun%modal%'
			and (lower(tweet_data.tweet) like '%must%'
			or lower(tweet_data.tweet) like '%should%')
		group by
			1);

-- final report
select
	profile.twitter_id,
	profile.gender,
	coalesce(assertive.total_count, 0)
from
	twitter_profile profile
left join assertive_tweets assertive on
	profile.twitter_id = assertive.twitter_id
order by
	1;
     