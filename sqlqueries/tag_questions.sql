create table if not exists tag_questions (twitter_id varchar(300),total_count int);
truncate table tag_questions;
--tag questions filtered data
select
	profile.twitter_id,
	tweet.tweet,
	array_agg(tag.sub_category)
from
	twitter_profile profile
	join tweet_data tweet on
	profile.twitter_id = tweet.twitter_id
	join tweet_pos_classification pos on
	pos.twitter_profile_id = tweet.twitter_id
	and tweet.tweet_number = pos.tweet_id
join tag_pos_mapping tag on
	tag.id = pos.tag_id
where
	tag.category = 'interrogatives'
	and tweet.tweet ~ '[.,]+[,\t\s\w]+[\?]+'
group by	1,2 order by 1;

--to get profile wise summary
insert into tag_questions   
(select
	profile.twitter_id,
	count(tweet.id)
from
	twitter_profile profile
	join tweet_data tweet on
	profile.twitter_id = tweet.twitter_id
	join tweet_pos_classification pos on
	pos.twitter_profile_id = tweet.twitter_id
	and tweet.tweet_number = pos.tweet_id
join tag_pos_mapping tag on
	tag.id = pos.tag_id
where
	tag.category = 'interrogatives'
	and tweet.tweet ~ '[.,]+[,\t\s\w]+[\?]+'
group by	1 );


--final report
select
	profile.twitter_id,
	profile.gender,
	coalesce(tag.total_count, 0)
from
	twitter_profile profile
left join tag_questions tag on
	profile.twitter_id = tag.twitter_id
order by
	1;